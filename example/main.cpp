#include "MainWindow.h"

#include <QApplication>
#include <QJsonDocument>
#include <QDebug>

#include "JsonQPropertyHelper.h"
#include "TestingQObject.h"
#include "HelperQObject.h"

#include "fjsondeserializer.h"

void test() {
    HelperQObject *tqobj = new HelperQObject();
    tqobj->setValue(2);
    tqobj->setValue2("2");
    TestingQObject *qobj = new TestingQObject(1);

    JsonQPropertyReader reader;
    reader.readProperties(qobj);
    QJsonObject jobj = reader.jsonObject();
    QJsonDocument doc(jobj);

    qDebug() << qPrintable(doc.toJson(QJsonDocument::JsonFormat::Indented));

    qDebug() << "qobj == jobj: " << ((*qobj) == jobj);
}

void testJsonSerializer() {
    FJsonSerializer serializer;
    QObject *qObj = new TestingQObject();
    QJsonObject jObj;
    jObj.insert("intvalue", 1);
    jObj.insert("doublevalue", 2.5);

    bool ok = serializer.deserialize(qObj, jObj);
    qDebug() << ok;
}

int main(int argc, char *argv[])
{

    testJsonSerializer();
    return 0;

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
