#include "TestingQObject.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QVariant>
#include <QVariantList>

#include <QDebug>

#define QSTR QStringLiteral

TestingQObject::TestingQObject(uint depth) : QObject(nullptr)
{
    m_testingObject = depth ? new TestingQObject(depth - 1) : nullptr;
}


QPoint TestingQObject::qpoint() const
{
    return m_qpoint;
}

QRect TestingQObject::qrect() const
{
    return m_qrect;
}

QRectF TestingQObject::qrectf() const
{
    return m_qrectf;
}

QChar TestingQObject::qchar() const
{
    return m_qChar;
}

QString TestingQObject::qstring() const
{
    return m_qString;
}

QSize TestingQObject::qsize() const
{
    return m_qSize;
}

QSizeF TestingQObject::qsizef() const
{
    return m_qSizeF;
}

QTime TestingQObject::qtime() const
{
    return m_qTime;
}

QDate TestingQObject::qdate() const
{
    return m_qDate;
}

QDateTime TestingQObject::qdatetime() const
{
    return m_qDateTime;
}

QLine TestingQObject::qline() const
{
    return m_qLine;
}

QLineF TestingQObject::qlinef() const
{
    return m_qLineF;
}

QStringList TestingQObject::qstringlist() const
{
    return m_qStringList;
}

QUrl TestingQObject::qurl() const
{
    return m_qUrl;
}

QColor TestingQObject::qcolor() const
{
    return m_qColor;
}

QFont TestingQObject::qfont() const
{
    return m_qFont;
}

QPolygon TestingQObject::qpolygon() const
{
    return m_qpolygon;
}

QPolygonF TestingQObject::qpolygonf() const
{
    return m_qpolygonf;
}


QKeySequence TestingQObject::qkeysequence() const
{
    return m_qkeysequence;
}

TestingQObject *TestingQObject::testingObject() const
{
    return m_testingObject;
}

QTimeZone TestingQObject::qtimezone() const
{
    return m_qtimezone;
}

qint32 TestingQObject::intvalue() const
{
    return m_intvalue;
}

double TestingQObject::doublevalue() const
{
    return m_doublevalue;
}

QPointF TestingQObject::qpointf() const
{
    return m_qpointf;
}

void TestingQObject::setQPoint(QPoint qpoint)
{
    m_qpoint = qpoint;
}

void TestingQObject::setQRect(QRect qrect)
{
    m_qrect = qrect;
}

void TestingQObject::setQRectF(QRectF qrectf)
{
    m_qrectf = qrectf;
}

void TestingQObject::setQChar(QChar qChar)
{
    m_qChar = qChar;
}

void TestingQObject::setQString(QString qString)
{
    m_qString = qString;
}

void TestingQObject::setQSize(QSize qSize)
{
    m_qSize = qSize;
}

void TestingQObject::setQSizeF(QSizeF qSizeF)
{
    m_qSizeF = qSizeF;
}

void TestingQObject::setQTime(QTime qTime)
{
    m_qTime = qTime;
}

void TestingQObject::setQDate(QDate qDate)
{
    m_qDate = qDate;
}

void TestingQObject::setQDateTime(QDateTime qDateTime)
{
    m_qDateTime = qDateTime;
}

void TestingQObject::setQLine(QLine qLine)
{
    m_qLine = qLine;
}

void TestingQObject::setQLineF(QLineF qLineF)
{
    m_qLineF = qLineF;
}

void TestingQObject::setQStringList(QStringList qStringList)
{
    m_qStringList = qStringList;
}

void TestingQObject::setQUrl(QUrl qUrl)
{
    m_qUrl = qUrl;
}

void TestingQObject::setQColor(QColor qColor)
{
    m_qColor = qColor;
}

void TestingQObject::setQFont(QFont qFont)
{
    m_qFont = qFont;
}

void TestingQObject::setQPolygon(QPolygon qpolygon)
{
    m_qpolygon = qpolygon;
}

void TestingQObject::setQPolygonF(QPolygonF qpolygonf)
{
    m_qpolygonf = qpolygonf;
}

void TestingQObject::setQKeySequence(QKeySequence qkeysequence)
{
    m_qkeysequence = qkeysequence;
}

void TestingQObject::setTestingObject(TestingQObject *testingObject)
{
    m_testingObject = testingObject;
}

void TestingQObject::setqtimezone(QTimeZone qtimezone)
{
    m_qtimezone = qtimezone;
}

void TestingQObject::setIntvalue(qint32 intvalue)
{
    m_intvalue = intvalue;
}

void TestingQObject::setDoublevalue(double doublevalue)
{
    m_doublevalue = doublevalue;
}

void TestingQObject::setQByteArray(QByteArray qByteArray)
{
    m_qByteArray = qByteArray;
}

void TestingQObject::setQPointF(QPointF qpointf)
{
    m_qpointf = qpointf;
}


QByteArray TestingQObject::qbytearray() const
{
    return m_qByteArray;
}

bool TestingQObject::operator ==(const QJsonObject &jobj)
{
    auto db_eq = [](double a, double b) {
        return std::abs(a - b) < 0.00001;
    };

    QPolygon jpolygon;
    {
        QJsonValue polygon = jobj["qpolygon"];
        if (polygon.isArray()) {
            QJsonArray arr = jobj["qpolygon"].toArray();
            for (int i = 0; i < arr.count();  ++i) {
                QJsonValue val = arr.at(i);
                jpolygon.append( QPoint((val >> "x").toInt(), (val >> "y").toInt()) );
            }
        }
    }

    bool qpolygonfOk = true;
    {
        QJsonValue polygonf = jobj["qpolygonf"];
        if (polygonf.isArray()) {
            QJsonArray arr = jobj["qpolygonf"].toArray();
            if (arr.count() == qpolygonf().count()) {
                for (int i = 0; i < arr.count();  ++i) {
                    QJsonValue val = arr.at(i);
                    auto x = (val >> "x").toDouble();
                    auto y = (val >> "y").toDouble();
                    QPointF pf = m_qpolygonf.at(i);
                    qpolygonfOk &= db_eq(pf.x(), x) && db_eq(pf.y(), y);
                }
            } else {
                qpolygonfOk = false;
            }
        }
    }

    QFont jfont;
    QJsonObject jTestingObject = jobj["testingObject"].toObject();

    bool equal = jfont.fromString((jobj >> "qfont").toString()) // test reading QFont
                 && qint64(jobj["intvalue"].toDouble()) == intvalue() // 0
                 && db_eq(jobj["doublevalue"].toDouble(), doublevalue()) // 0

                 && QString(qchar()) == jobj[QStringLiteral("qchar")].toString() // 1
                 && qstring() == jobj[QStringLiteral("qstring")].toString()      // 2
                 && qbytearray() == QByteArray::fromBase64(jobj[QStringLiteral("qbytearray")].toString().toLatin1()) // 3
                 && QJsonArray::fromStringList( qstringlist() ) == jobj["qstringlist"].toArray() // 4

                 && QPoint((jobj >> "qpoint" >> "x").toInt(), // 5
                           (jobj >> "qpoint" >> "y").toInt()) == qpoint()

                 && db_eq((jobj >> "qpointf" >> "x").toDouble() , qpointf().x()) // 6
                 && db_eq((jobj >> "qpointf" >> "y").toDouble() , qpointf().y())

                 && QRect((jobj >> "qrect" >> "x").toInt(), (jobj >> "qrect" >> "y").toInt(), // 7
                          (jobj >> "qrect" >> "width").toInt(), (jobj >> "qrect" >> "height").toInt()) == qrect()

                 && db_eq((jobj >> "qrectf" >> "x").toDouble(), qrectf().x()) // 8
                 && db_eq((jobj >> "qrectf" >> "y").toDouble(), qrectf().y())
                 && db_eq((jobj >> "qrectf" >> "width").toDouble(), qrectf().width())
                 && db_eq((jobj >> "qrectf" >> "height").toDouble(), qrectf().height())

                 && QSize((jobj >> "qsize" >> "width").toInt(), // 9
                          (jobj >> "qsize" >> "height").toInt()) == qsize()

                 && db_eq((jobj >> "qsizef" >> "width").toDouble(), qsizef().width()) // 10
                 && db_eq((jobj >> "qsizef" >> "height").toDouble(), qsizef().height())

                 && QTime::fromString((jobj >> "qtime").toString(), QSTR("hh:mm:ss:zzz")) == qtime() // 11
                 && QDate::fromString((jobj >> "qdate").toString(), Qt::DateFormat::ISODate) == qdate() // 12
                 && QDateTime::fromString((jobj >> "qdatetime").toString(), Qt::DateFormat::ISODate) == qdatetime().toUTC() // 13
                 && QTimeZone((jobj >> "qtimezone").toString().toLatin1()) == qtimezone() // 14

                 && QLine((jobj >> "qline" >> "x1").toInt(), (jobj >> "qline" >> "y1").toInt(), // 15
                          (jobj >> "qline" >> "x2").toInt(), (jobj >> "qline" >> "y2").toInt()) == qline()

                 && db_eq((jobj >> "qlinef" >> "x1").toDouble(), qlinef().x1()) // 16
                 && db_eq((jobj >> "qlinef" >> "y1").toDouble(), qlinef().y1())
                 && db_eq((jobj >> "qlinef" >> "x2").toDouble(), qlinef().x2())
                 && db_eq((jobj >> "qlinef" >> "y2").toDouble(), qlinef().y2())

                 && QUrl((jobj >> "qurl").toString()) == qurl() // 17
                 && QColor((jobj >> "qcolor").toString()) == qcolor() // 18
                 && jfont == qfont() // 19

                 && jpolygon == qpolygon() // 20

                 && qpolygonfOk // 21

                 && QKeySequence::fromString((jobj >> "qkeysequence").toString()) == qkeysequence() // 22

                 && (testingObject() ? ((*testingObject()) == jTestingObject) : jobj["testingObject"].isNull()) // 23
                 ;

    return equal;
}


const QJsonValue operator >>(const QJsonValue &jvalue, const QString &key)
{
    Q_ASSERT(jvalue.isObject());
    return jvalue.toObject()[key];
}

const QJsonValue operator >>(const QJsonObject &jvalue, const QString &key)
{
    return jvalue[key];
}

//const QJsonValue operator >>(const QJsonValue &jvalue, const QLatin1String &key)
//{
//    Q_ASSERT(jvalue.isObject());
//    return jvalue.toObject()[key];
//}

//const QJsonValue operator >>(const QJsonObject &jvalue, const QLatin1String &key)
//{
//    return jvalue[key];
//}
