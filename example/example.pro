#-------------------------------------------------
#
# Project created by QtCreator 2015-11-04T22:39:30
#
#-------------------------------------------------

QT       += core gui

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = example
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp \
    TestingQObject.cpp

HEADERS  += MainWindow.h \
    TestingQObject.h

FORMS    += MainWindow.ui

include(../src/src.pri)
