#ifndef TESTINGQOBJECT_H
#define TESTINGQOBJECT_H

#include <QObject>
#include <QJsonObject>
#include <QJsonValue>

#include <QChar>
#include <QString>
#include <QStringList>

#include <QRect>
#include <QPoint>
#include <QSize>
#include <QLine>

#include <QTime>
#include <QDate>
#include <QDateTime>
#include <QTimeZone>

#include <QByteArray>
#include <QUrl>

// GUI
#include <QColor>
#include <QFont>

#include <QPolygon>
#include <QMatrix>

#include <QKeySequence>


#include "JsonQMetaTypeHelper.h"

class TestingQObject : public QObject
{
    Q_OBJECT

    Q_PROPERTY(qint32 intvalue READ intvalue WRITE setIntvalue) // 0
    Q_PROPERTY(double doublevalue READ doublevalue WRITE setDoublevalue) // 0

    Q_PROPERTY(QChar qchar READ qchar WRITE setQChar) // 1
    Q_PROPERTY(QString qstring READ qstring WRITE setQString) // 2
    Q_PROPERTY(QByteArray qbytearray READ qbytearray WRITE setQByteArray) // 3
    Q_PROPERTY(QStringList qstringlist READ qstringlist WRITE setQStringList) // 4

    Q_PROPERTY(QPoint qpoint READ qpoint WRITE setQPoint) // 5
    Q_PROPERTY(QPointF qpointf READ qpointf WRITE setQPointF) // 6

    Q_PROPERTY(QRect qrect READ qrect WRITE setQRect) // 7
    Q_PROPERTY(QRectF qrectf READ qrectf WRITE setQRectF) // 8

    Q_PROPERTY(QSize qsize READ qsize WRITE setQSize) // 9
    Q_PROPERTY(QSizeF qsizef READ qsizef WRITE setQSizeF) // 10

    Q_PROPERTY(QTime qtime READ qtime WRITE setQTime) // 11
    Q_PROPERTY(QDate qdate READ qdate WRITE setQDate) // 12
    Q_PROPERTY(QDateTime qdatetime READ qdatetime WRITE setQDateTime) // 13
    Q_PROPERTY(QTimeZone qtimezone READ qtimezone WRITE setqtimezone) // 14

    Q_PROPERTY(QLine qline READ qline WRITE setQLine) // 15
    Q_PROPERTY(QLineF qlinef READ qlinef WRITE setQLineF) // 16

    Q_PROPERTY(QUrl qurl READ qurl WRITE setQUrl) // 17

    // GUI
    Q_PROPERTY(QColor qcolor READ qcolor WRITE setQColor) // 18
    Q_PROPERTY(QFont qfont READ qfont WRITE setQFont) // 19

    Q_PROPERTY(QPolygon qpolygon READ qpolygon WRITE setQPolygon) // 20
    Q_PROPERTY(QPolygonF qpolygonf READ qpolygonf WRITE setQPolygonF) // 21

    Q_PROPERTY(QKeySequence qkeysequence READ qkeysequence WRITE setQKeySequence) // 22

    Q_PROPERTY(TestingQObject *testingObject READ testingObject WRITE setTestingObject) // 23

public:
    explicit TestingQObject(uint depth = 0);

    bool operator ==(const QJsonObject &jobj);

    QChar qchar() const;
    QString qstring() const;
    QByteArray qbytearray() const;

    QPoint qpoint() const;
    QPointF qpointf() const;

    QRect qrect() const;
    QRectF qrectf() const;


    QSize qsize() const;
    QSizeF qsizef() const;

    QTime qtime() const;
    QDate qdate() const;
    QDateTime qdatetime() const;

    QLine qline() const;
    QLineF qlinef() const;


    QStringList qstringlist() const;

    QUrl qurl() const;

    // GUI
    QColor qcolor() const;
    QFont qfont() const;

    QPolygon qpolygon() const;
    QPolygonF qpolygonf() const;

    QKeySequence qkeysequence() const;

    TestingQObject *testingObject() const;

    QTimeZone qtimezone() const;


    qint32 intvalue() const;
    double doublevalue() const;

public slots:

    void setQChar(QChar qchar);
    void setQString(QString qstring);
    void setQByteArray(QByteArray qbytearray);

    void setQPoint(QPoint qpoint);
    void setQPointF(QPointF qpointf);

    void setQRect(QRect qrect);
    void setQRectF(QRectF qrectf);

    void setQSize(QSize qsize);
    void setQSizeF(QSizeF qsizef);

    void setQTime(QTime qtime);
    void setQDate(QDate qdate);
    void setQDateTime(QDateTime qdatetime);

    void setQLine(QLine qline);
    void setQLineF(QLineF qlinef);

    void setQStringList(QStringList qstringlist);

    void setQUrl(QUrl qurl);

    // GUI
    void setQColor(QColor qcolor);
    void setQFont(QFont qfont);

    void setQPolygon(QPolygon qpolygon);
    void setQPolygonF(QPolygonF qpolygonf);

    void setQKeySequence(QKeySequence qkeysequence);

    void setTestingObject(TestingQObject *testingObject);

    void setqtimezone(QTimeZone qtimezone);

    void setIntvalue(qint32 intvalue);

    void setDoublevalue(double doublevalue);


private:

    qint32 m_intvalue = std::numeric_limits<qint32>::max();
    double m_doublevalue  = std::numeric_limits<double>::max();

    QChar m_qChar           { QLatin1Char('J') };
    QString m_qString       { QLatin1String("QString") };
    QByteArray m_qByteArray { QByteArrayLiteral("QByteArray") };
    QStringList m_qStringList { QStringLiteral("str1"), QStringLiteral("str2"), QStringLiteral("str2")};

    QPoint m_qpoint         { 1, 2 };
    QPointF m_qpointf       { 10.1, 20.1 };

    QRect m_qrect           { 10, 10, 100, 100 };
    QRectF m_qrectf         { 100.1, 200.2, 300.3, 400.4 };

    QSize m_qSize           { 10, 10 };
    QSizeF m_qSizeF         { 10.1, 20.2 };

    QTime m_qTime           { QTime(10, 20, 30, 400) };
    QDate m_qDate           { QDate(2010, 11, 22) };
    QDateTime m_qDateTime   { QDateTime( QDate(2010, 11, 22), QTime(10, 20, 30 , 000), QTimeZone("Europe/Kiev")) };

    QLine m_qLine           { QLine(1, 2, 3, 4) };
    QLineF m_qLineF         { QLineF(1.1, 2.2, 3.3, 4.4) };

    QUrl m_qUrl             { QUrl("http://qt.io") };

    QColor m_qColor         { QColor("Red") };
    QFont m_qFont           { QFont("Times New Roman", 10, QFont::Bold) };

    QPolygon m_qpolygon     { QPolygon( { QPoint(1, 2), QPoint(3, 4), QPoint(5, 6) } ) };
    QPolygonF m_qpolygonf   { QPolygonF( { QPointF(11.1, 22.2), QPointF(33.3, 44.4), QPointF(55.5, 66.6) } ) };

    QKeySequence m_qkeysequence { QLatin1String("Ctrl+X") };

    QTimeZone m_qtimezone   { QTimeZone("Europe/Kiev") };

    TestingQObject *m_testingObject = nullptr;
};

const QJsonValue operator >>(const QJsonValue &jvalue, const QString &key);
const QJsonValue operator >>(const QJsonObject &jvalue, const QString &key);

//const QJsonValue operator >>(const QJsonValue &jvalue, const QLatin1String &key);
//const QJsonValue operator >>(const QJsonObject &jvalue, const QLatin1String &key);

#endif // TESTINGQOBJECT_H
