CONFIG += c++11

INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/JsonQPropertyHelper.h \
    $$PWD/JsonQMetaTypeHelper.h \
    $$PWD/HelperQObject.h \
    $$PWD/fjsondeserializer.h

SOURCES += \
    $$PWD/JsonQPropertyHelper.cpp \
    $$PWD/HelperQObject.cpp \
    $$PWD/fjsondeserializer.cpp
