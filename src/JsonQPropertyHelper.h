#ifndef JSONQPROPERTYHELPER_H
#define JSONQPROPERTYHELPER_H

#include <type_traits>

#include <QObject>
#include <QJsonObject>

#include "JsonQMetaTypeHelper.h"

#define QL1STR QLatin1String

namespace std {
template<typename T, typename Head, typename... Tail>
struct is_one_of
        : public std::integral_constant<bool,
                                        std::is_same<T, Head>::value
                                        || is_one_of<T, Tail...>::value > {

};

template<typename T, typename Head>
struct is_one_of<T, Head>
        : public std::integral_constant<bool,
                                        std::is_same<T, Head>::value> {

};

template <typename T>
struct is_one {
    template <typename ...List>
    struct of {
        static const bool value = std::is_one_of<T, List...>::value;
    };
};

}

class QMetaProperty;

class JsonQPropertyHelper
{
public:
    JsonQPropertyHelper();
};

template<class T, class ...A>
using enable_if_one_of = typename std::enable_if < std::is_one_of<T, A...>::value, T >::type;

class JsonQPropertyReader
{
public:

    QString errorString() const;

    bool readProperties(const QObject *qobject, const QStringList &ignoredProperties = QStringList());
    QJsonObject jsonObject() const;

    bool saveToFile(const QString &fileName);

protected:

    QJsonObject properties2jsonobject(const QObject *qobject);
    virtual QJsonValue qproperty2jsonvalue(const QObject *qobject, QMetaProperty &metaProperty);

private:
    template<typename T, enable_if_one_of<T, QPoint, QPointF>* = nullptr>
    QJsonValue jsonValue(const T &point);

    template <typename T, enable_if_one_of<T, QRect, QRectF>* = nullptr>
    QJsonValue jsonValue(const T &rect);

    template <typename T, enable_if_one_of<T, QSize, QSizeF>* = nullptr>
    QJsonValue jsonValue(const T &size);

    template <typename T, enable_if_one_of<T, QLine, QLineF>* = nullptr>
    QJsonValue jsonValue(const T &line);

    template <typename T, enable_if_one_of<T, QPolygon, QPolygonF>* = nullptr>
    QJsonValue jsonValue(const T &polygon);

    template <typename T, enable_if_one_of<T, QByteArray>* = nullptr>
    QJsonValue jsonValue(const T &bytearray);

    QJsonValue jsonValueFromUserType(const QVariant &vProperty);

private:
    QStringList m_ignoredProperties;
    QJsonObject m_jsonObject;
    QString m_errorString;
};

#endif // JSONQPROPERTYHELPER_H
