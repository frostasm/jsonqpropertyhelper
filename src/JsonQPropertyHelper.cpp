#include "JsonQPropertyHelper.h"

#include <QDebug>

#include <QJsonArray>

#include <QMetaObject>
#include <QMetaProperty>
#include <QDebug>

#include <QRect>
#include <QLine>
#include <QPolygon>

#include <QDateTime>
#include <QByteArray>
#include <QKeySequence>

#include <QTimeZone>


JsonQPropertyHelper::JsonQPropertyHelper()
{

}

QString JsonQPropertyReader::errorString() const
{
    return m_errorString;
}

bool JsonQPropertyReader::readProperties(const QObject *qobject, const QStringList &ignoredProperties)
{
    m_ignoredProperties = ignoredProperties;
    QJsonObject jobject = properties2jsonobject(qobject);
    m_jsonObject = jobject;
    Q_UNUSED(jobject);
    return false;
}

QJsonObject JsonQPropertyReader::jsonObject() const
{
    return m_jsonObject;
}

bool JsonQPropertyReader::saveToFile(const QString &fileName)
{
    Q_UNUSED(fileName);
    m_errorString.clear();
    // TODO: add realization
    return false;
}

QJsonObject JsonQPropertyReader::properties2jsonobject(const QObject *qobject)
{
    QJsonObject jobject;
    const QMetaObject *metaObject = qobject->metaObject();

    int count = metaObject->propertyCount();
    for (int i = 0; i < count; ++i) {
        QMetaProperty metaProperty = metaObject->property(i);
        const char *cname = metaProperty.name();

        QLatin1Literal name = QLatin1String(cname);
        bool skipProperty = !metaProperty.isReadable() ||!metaProperty.isStored() || m_ignoredProperties.contains(name);
        if (skipProperty)
            continue;

        QJsonValue jsonValue = qproperty2jsonvalue(qobject, metaProperty);
        jobject.insert(name, jsonValue);
    }

    return jobject;
}

QJsonValue JsonQPropertyReader::qproperty2jsonvalue(const QObject *qobject, QMetaProperty &metaProperty)
{

    QMetaType::Type propertyType = QMetaType::Type(metaProperty.type());
    QVariant vProperty = metaProperty.read(qobject);
    QJsonValue jValue(QJsonValue::Undefined);

//    qDebug() << metaProperty.name() << propertyType << vProperty;

    switch (propertyType) {

    case QMetaType::Type::Bool:

    case QMetaType::Type::Char:
    case QMetaType::Type::SChar:
    case QMetaType::Type::UChar:

    case QMetaType::Type::Short:
    case QMetaType::Type::UShort:

    case QMetaType::Type::Int:
    case QMetaType::Type::UInt:

    case QMetaType::Type::Long:
    case QMetaType::Type::ULong:

    case QMetaType::Type::LongLong:
    case QMetaType::Type::ULongLong:

    case QMetaType::Type::Float:
    case QMetaType::Type::Double:

    case QMetaType::Type::QString:
    case QMetaType::Type::QChar:

    case QMetaType::Type::QStringList:

    case QMetaType::Type::QColor:
    case QMetaType::Type::QFont:
        // TODO: add object?

    case QMetaType::Type::QUrl:
        // default convertation
        jValue = QJsonValue::fromVariant(vProperty);
        break;

    case QMetaType::Type::QPoint:
        jValue = jsonValue(vProperty.toPoint());
        break;

    case QMetaType::Type::QPointF:
        jValue = jsonValue(vProperty.toPointF());
        break;

    case QMetaType::Type::QRect:
        jValue = jsonValue(vProperty.toRect());
        break;

    case QMetaType::Type::QRectF:
        jValue = jsonValue(vProperty.toRectF());
        break;

    case QMetaType::Type::QSize:
        jValue = jsonValue(vProperty.toSize());
        break;

    case QMetaType::Type::QSizeF:
        jValue = jsonValue(vProperty.toSizeF());
        break;

    case QMetaType::Type::QLine:
        jValue = jsonValue(vProperty.toLine());
        break;

    case QMetaType::Type::QLineF:
        jValue = jsonValue(vProperty.toLineF());
        break;

    case QMetaType::Type::QTime:
        jValue = vProperty.toTime().toString("hh:mm:ss:zzz");
        break;

    case QMetaType::Type::QDate:
        jValue = vProperty.toDate().toString(Qt::DateFormat::ISODate);
        break;

    case QMetaType::Type::QDateTime:
        jValue = vProperty.toDateTime().toUTC().toString(Qt::DateFormat::ISODate);
        break;

    case QMetaType::Type::QByteArray:
        jValue = jsonValue( vProperty.toByteArray() );
        break;

    case QMetaType::Type::User:
        jValue = jsonValueFromUserType(vProperty);
        break;

    // GUI
    case QMetaType::Type::QPolygon:
        jValue = jsonValue(vProperty.value<QPolygon>());
        break;

    case QMetaType::Type::QPolygonF:
        jValue = jsonValue(vProperty.value<QPolygonF>());
        break;

    case QMetaType::Type::QKeySequence:
        jValue = vProperty.value<QKeySequence>().toString(QKeySequence::SequenceFormat::PortableText);
        break;

    default:
        // invalid
        break;
    };

    return jValue;
}

QJsonValue JsonQPropertyReader::jsonValueFromUserType(const QVariant &vProperty)
{
    QJsonValue jValue(QJsonValue::Undefined);
    if (vProperty.canConvert<QObject*>()) {
        QObject *obj = vProperty.value<QObject *>();
        jValue = obj ? properties2jsonobject(obj) : QJsonValue(QJsonValue::Null);
    } else if (vProperty.canConvert<QTimeZone>() ) {
        QTimeZone timeZone = vProperty.value<QTimeZone>();
        jValue = QString::fromLatin1(timeZone.id());
    }
    return jValue;
}

template<typename T, enable_if_one_of<T, QPoint, QPointF>*>
inline QJsonValue JsonQPropertyReader::jsonValue(const T &point)
{
    QJsonObject value {
        { QL1STR("x"), point.x() },
        { QL1STR("y"), point.y() },
    };
    return value;
}

template <typename T, enable_if_one_of<T, QRect, QRectF>*>
inline QJsonValue JsonQPropertyReader::jsonValue(const T &rect)
{
    QJsonObject value {
        { QL1STR("x"), rect.x() },
        { QL1STR("y"), rect.y() },
        { QL1STR("width"), rect.width() },
        { QL1STR("height"), rect.height() },
    };
    return value;
}

template <typename T, enable_if_one_of<T, QSize, QSizeF>*>
inline QJsonValue JsonQPropertyReader::jsonValue(const T &size)
{
    QJsonObject value {
        { QL1STR("width"), size.width() },
        { QL1STR("height"), size.height() },
    };
    return value;
}

template <typename T, enable_if_one_of<T, QLine, QLineF>*>
inline QJsonValue JsonQPropertyReader::jsonValue(const T &line)
{
    QJsonObject value {
        { QL1STR("x1"), line.x1() },
        { QL1STR("y1"), line.y1() },
        { QL1STR("x2"), line.x2() },
        { QL1STR("y2"), line.y2() },
    };
    return value;
}

template <typename T, enable_if_one_of<T, QPolygon, QPolygonF>*>
inline QJsonValue JsonQPropertyReader::jsonValue(const T &polygon)
{
    QJsonArray array;
    int count = polygon.count();
    for (int i = 0; i < count; ++i)
        array.append( jsonValue(polygon.at(i)) );

    return array;
}

template <typename T, enable_if_one_of<T, QByteArray>*>
inline QJsonValue JsonQPropertyReader::jsonValue(const T &bytearray)
{
    return QString::fromLatin1(bytearray.toBase64());
}

