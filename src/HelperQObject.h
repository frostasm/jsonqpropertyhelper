#ifndef HELPERQOBJECT_H
#define HELPERQOBJECT_H

#include <QObject>

class HelperQObject : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int value READ value WRITE setValue NOTIFY valueChanged)
    Q_PROPERTY(QString value2 READ value2 WRITE setValue2 NOTIFY valueChanged2)

public:
    explicit HelperQObject(QObject *parent = 0);

    int value() const;

    QString value2() const;

signals:
    void valueChanged(int value);
    void valueChanged2(QString value2);

public slots:
    void setValue(int value);
    void somePropertyChanged();

    void setValue2(QString value2);

private:
    int m_value;
    QString m_value2;
};

#endif // HELPERQOBJECT_H
