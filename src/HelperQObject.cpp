#include "HelperQObject.h"

#include <QMetaObject>
#include <QDebug>
#include <QMetaProperty>

HelperQObject::HelperQObject(QObject *parent) : QObject(parent)
{
    const QMetaObject *metaObject = this->metaObject();
    int deleteLaterIndex = metaObject->indexOfMethod(metaObject->normalizedSignature("somePropertyChanged()"));

    QMetaMethod valueChangedMetaMethod = metaObject->method(deleteLaterIndex);

    int count = metaObject->propertyCount();
    for (int i = 0; i < count; ++i) {
        QMetaProperty metaProperty = metaObject->property(i);
//        const char *cname = metaProperty.name();

        if (metaProperty.hasNotifySignal())
            QObject::connect(this, metaProperty.notifySignal(), this, valueChangedMetaMethod);
    }

}

int HelperQObject::value() const
{
    return m_value;
}

void HelperQObject::setValue(int value)
{
    if (m_value == value)
        return;

    m_value = value;
    emit valueChanged(value);
}

void HelperQObject::somePropertyChanged()
{
    qDebug() << sender();
}

QString HelperQObject::value2() const
{
    return m_value2;
}

void HelperQObject::setValue2(QString value2)
{

    if (m_value2 == value2)
        return;

    m_value2 = value2;
    emit valueChanged2(value2);
}
