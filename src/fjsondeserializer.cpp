#include "fjsondeserializer.h"

#include <QObject>

#include <QMetaObject>
#include <QMetaProperty>

#include <QJsonObject>
#include <QJsonValue>

#include <QDebug>

FJsonSerializer::FJsonSerializer()
{

}

bool FJsonSerializer::deserialize(QObject *qObj, const QJsonObject &jObj)
{
    bool allOk = true;
    const QMetaObject *metaObject = qObj->metaObject();
    const int count = metaObject->propertyCount();
    for (int i = 0; i < count; ++i) {
        QMetaProperty metaProperty = metaObject->property(i);
        QString name(metaProperty.name());

        if (jObj.contains(name) && metaProperty.isWritable()) {
            QVariant variant = jvalue2qvalue(metaProperty, jObj.value(name));
            allOk &= variant.isValid() && qObj->setProperty(metaProperty.name(), variant);
        }
    }

    return allOk;
}

QVariant FJsonSerializer::jvalue2qvalue(QVariant::Type valueType, const QJsonValue &jValue)
{
    // TODO: rewrite this monster :)
    QVariant vProperty = QVariant();
    QMetaType::Type metaType = static_cast<QMetaType::Type>(valueType);

    switch (metaType) {

    // unsinged integers
    case QMetaType::UChar:
    case QMetaType::UShort:
    case QMetaType::UInt:
    case QMetaType::ULong:
    case QMetaType::ULongLong:
        if (jValue.type() == QJsonValue::Type::Double)
            vProperty.setValue(quint64(jValue.toDouble()));
        break;

    // singed integers
    case QMetaType::Char:
    case QMetaType::SChar:
    case QMetaType::Short:
    case QMetaType::Int:
    case QMetaType::Long:
    case QMetaType::LongLong:
        if (jValue.type() == QJsonValue::Type::Double)
            vProperty.setValue(qint64(jValue.toDouble()));
        break;

    case QMetaType::Float:
    case QMetaType::Double:
        if (jValue.type() == QJsonValue::Type::Double)
            vProperty.setValue(double(jValue.toDouble()));
        break;

    case QMetaType::QString:
        if (jValue.type() == QJsonValue::Type::String)
            vProperty.setValue(jValue.toString());
        break;


    case QMetaType::User:
        // TODO: add realization

    default:
        return QVariant();
    }

    return vProperty;
}
