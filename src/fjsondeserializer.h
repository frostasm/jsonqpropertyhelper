#ifndef FJSONDESERIALIZER_H
#define FJSONDESERIALIZER_H

#include <QVariant>

class QObject;
class QjsonValue;

class FJsonSerializer
{
public:
    FJsonSerializer();

    bool deserialize(QObject *qObj, const QJsonObject &jObj);

protected:
    virtual QVariant jvalue2qvalue(QVariant::Type valueType, const QJsonValue &jValue);
};

#endif // FJSONDESERIALIZER_H
