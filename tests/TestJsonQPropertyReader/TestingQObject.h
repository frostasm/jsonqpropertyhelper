#ifndef TESTINGQOBJECT_H
#define TESTINGQOBJECT_H

#include <QObject>

#include <QChar>
#include <QString>
#include <QStringList>

#include <QRect>
#include <QPoint>
#include <QSize>
#include <QLine>

#include <QTime>
#include <QDate>
#include <QDateTime>
#include <QTimeZone>

#include <QByteArray>
#include <QUrl>


// GUI
#include <QColor>
#include <QFont>

#include <QPolygon>
#include <QMatrix>

#include <QKeySequence>

class TestingQObject : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QChar qChar READ qChar WRITE setQChar)
    Q_PROPERTY(QString qString READ qString WRITE setQString)
    Q_PROPERTY(QByteArray qByteArray READ qByteArray WRITE setQByteArray)
    Q_PROPERTY(QStringList qStringList READ qStringList WRITE setQStringList)

    Q_PROPERTY(QPoint qpoint READ qpoint WRITE setQPoint)
    Q_PROPERTY(QPointF qpointf READ qpointf WRITE setQPointF)

    Q_PROPERTY(QRect qrect READ qrect WRITE setQRect)
    Q_PROPERTY(QRectF qrectf READ qrectf WRITE setQRectF)

    Q_PROPERTY(QSize qSize READ qSize WRITE setQSize)
    Q_PROPERTY(QSizeF qSizeF READ qSizeF WRITE setQSizeF)

    Q_PROPERTY(QTime qTime READ qTime WRITE setQTime)
    Q_PROPERTY(QDate qDate READ qDate WRITE setQDate)
    Q_PROPERTY(QDateTime qDateTime READ qDateTime WRITE setQDateTime)

    Q_PROPERTY(QLine qLine READ qLine WRITE setQLine)
    Q_PROPERTY(QLineF qLineF READ qLineF WRITE setQLineF)

    Q_PROPERTY(QUrl qUrl READ qUrl WRITE setQUrl)

    // GUI
    Q_PROPERTY(QColor qColor READ qColor WRITE setQColor)
    Q_PROPERTY(QFont qFont READ qFont WRITE setQFont)

    Q_PROPERTY(QPolygon qpolygon READ qpolygon WRITE setQPolygon)
    Q_PROPERTY(QPolygonF qpolygonf READ qpolygonf WRITE setQPolygonF)

    Q_PROPERTY(QKeySequence qkeysequence READ qkeysequence WRITE setQKeySequence)

    Q_PROPERTY(TestingQObject *testingObject READ testingObject WRITE setTestingObject)

public:
    explicit TestingQObject(uint depth = 0);

    QChar qChar() const;
    QString qString() const;
    QByteArray qByteArray() const;

    QPoint qpoint() const;
    QPointF qpointf() const;

    QRect qrect() const;
    QRectF qrectf() const;


    QSize qSize() const;
    QSizeF qSizeF() const;

    QTime qTime() const;
    QDate qDate() const;
    QDateTime qDateTime() const;

    QLine qLine() const;
    QLineF qLineF() const;


    QStringList qStringList() const;

    QUrl qUrl() const;

    // GUI
    QColor qColor() const;
    QFont qFont() const;

    QPolygon qpolygon() const;
    QPolygonF qpolygonf() const;

    QKeySequence qkeysequence() const;

    TestingQObject *testingObject() const;

public slots:

    void setQChar(QChar qChar);
    void setQString(QString qString);
    void setQByteArray(QByteArray qByteArray);

    void setQPoint(QPoint qpoint);
    void setQPointF(QPointF qpointf);

    void setQRect(QRect qrect);
    void setQRectF(QRectF qrectf);

    void setQSize(QSize qSize);
    void setQSizeF(QSizeF qSizeF);

    void setQTime(QTime qTime);
    void setQDate(QDate qDate);
    void setQDateTime(QDateTime qDateTime);

    void setQLine(QLine qLine);
    void setQLineF(QLineF qLineF);

    void setQStringList(QStringList qStringList);

    void setQUrl(QUrl qUrl);

    // GUI
    void setQColor(QColor qColor);
    void setQFont(QFont qFont);

    void setQPolygon(QPolygon qpolygon);
    void setQPolygonF(QPolygonF qpolygonf);

    void setQKeySequence(QKeySequence qkeysequence);

    void setTestingObject(TestingQObject *testingObject);

private:

    QChar m_qChar           { QLatin1Char('J') };
    QString m_qString       { QLatin1String("QString") };

    QPoint m_qpoint         { 1, 2 };
    QPointF m_qpointf       { 10.1, 20.1 };

    QRect m_qrect           { 10, 10, 100, 100 };
    QRectF m_qrectf         { 100.1, 200.2, 300.3, 400.4 };

    QSize m_qSize           { 10, 10 };
    QSizeF m_qSizeF         { 10.1, 20.2 };

    QTime m_qTime           { QTime(10, 20, 30, 400) };
    QDate m_qDate           { QDate(2010, 11, 22) };
    QDateTime m_qDateTime   { QDateTime( QDate(2010, 11, 22), QTime(10, 20, 30 , 400), QTimeZone("Europe/Kiev")) };
    QLine m_qLine           { QLine(1, 2, 3, 4) };
    QLineF m_qLineF         { QLineF(1.1, 2.2, 3.3, 4.4) };
    QByteArray m_qByteArray { QByteArrayLiteral("QByteArray") };
    QStringList m_qStringList { QStringLiteral("str1"), QStringLiteral("str2"), QStringLiteral("str2")};
    QUrl m_qUrl             { QUrl("http://qt.io") };


    QColor m_qColor         { QColor("Red") };
    QFont m_qFont           { QFont("Times New Roman", 10, QFont::Bold) };
    QPolygon m_qpolygon     { QPolygon( { QPoint(1, 2), QPoint(3, 4), QPoint(5, 6) } ) };
    QPolygonF m_qpolygonf   { QPolygonF( { QPointF(11.1, 22.2), QPointF(33.3, 44.4), QPointF(55.5, 66.6) } ) };
    QKeySequence m_qkeysequence { QLatin1String("Ctrl+X") };

    TestingQObject *m_testingObject = nullptr;
};

#endif // TESTINGQOBJECT_H
