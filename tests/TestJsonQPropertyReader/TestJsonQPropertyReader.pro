#-------------------------------------------------
#
# Project created by QtCreator 2015-11-04T22:41:55
#
#-------------------------------------------------

QT       += testlib

CONFIG += c++11

TARGET = test_JsonQPropertyReader
CONFIG   += console
CONFIG   -= app_bundle

CONFIG   += testcase

TEMPLATE = app

SOURCES += test_JsonQPropertyReader.cpp \
    TestingQObject.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

include(../../src/src.pri)

HEADERS += \
    TestingQObject.h
