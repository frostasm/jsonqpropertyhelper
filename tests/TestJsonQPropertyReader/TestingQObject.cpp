#include "TestingQObject.h"


TestingQObject::TestingQObject(uint depth) : QObject(nullptr)
{
    if (depth)
        m_testingObject = new TestingQObject(depth-1);
}


QPoint TestingQObject::qpoint() const
{
    return m_qpoint;
}

QRect TestingQObject::qrect() const
{
    return m_qrect;
}

QRectF TestingQObject::qrectf() const
{
    return m_qrectf;
}

QChar TestingQObject::qChar() const
{
    return m_qChar;
}

QString TestingQObject::qString() const
{
    return m_qString;
}

QSize TestingQObject::qSize() const
{
    return m_qSize;
}

QSizeF TestingQObject::qSizeF() const
{
    return m_qSizeF;
}

QTime TestingQObject::qTime() const
{
    return m_qTime;
}

QDate TestingQObject::qDate() const
{
    return m_qDate;
}

QDateTime TestingQObject::qDateTime() const
{
    return m_qDateTime;
}

QLine TestingQObject::qLine() const
{
    return m_qLine;
}

QLineF TestingQObject::qLineF() const
{
    return m_qLineF;
}

QStringList TestingQObject::qStringList() const
{
    return m_qStringList;
}

QUrl TestingQObject::qUrl() const
{
    return m_qUrl;
}

QColor TestingQObject::qColor() const
{
    return m_qColor;
}

QFont TestingQObject::qFont() const
{
    return m_qFont;
}

QPolygon TestingQObject::qpolygon() const
{
    return m_qpolygon;
}

QPolygonF TestingQObject::qpolygonf() const
{
    return m_qpolygonf;
}


QKeySequence TestingQObject::qkeysequence() const
{
    return m_qkeysequence;
}

TestingQObject *TestingQObject::testingObject() const
{
    return m_testingObject;
}

QPointF TestingQObject::qpointf() const
{
    return m_qpointf;
}

void TestingQObject::setQPoint(QPoint qpoint)
{
    m_qpoint = qpoint;
}

void TestingQObject::setQRect(QRect qrect)
{
    m_qrect = qrect;
}

void TestingQObject::setQRectF(QRectF qrectf)
{
    m_qrectf = qrectf;
}

void TestingQObject::setQChar(QChar qChar)
{
    m_qChar = qChar;
}

void TestingQObject::setQString(QString qString)
{
    m_qString = qString;
}

void TestingQObject::setQSize(QSize qSize)
{
    m_qSize = qSize;
}

void TestingQObject::setQSizeF(QSizeF qSizeF)
{
    m_qSizeF = qSizeF;
}

void TestingQObject::setQTime(QTime qTime)
{
    m_qTime = qTime;
}

void TestingQObject::setQDate(QDate qDate)
{
    m_qDate = qDate;
}

void TestingQObject::setQDateTime(QDateTime qDateTime)
{
    m_qDateTime = qDateTime;
}

void TestingQObject::setQLine(QLine qLine)
{
    m_qLine = qLine;
}

void TestingQObject::setQLineF(QLineF qLineF)
{
    m_qLineF = qLineF;
}

void TestingQObject::setQStringList(QStringList qStringList)
{
    m_qStringList = qStringList;
}

void TestingQObject::setQUrl(QUrl qUrl)
{
    m_qUrl = qUrl;
}

void TestingQObject::setQColor(QColor qColor)
{
    m_qColor = qColor;
}

void TestingQObject::setQFont(QFont qFont)
{
    m_qFont = qFont;
}

void TestingQObject::setQPolygon(QPolygon qpolygon)
{
    m_qpolygon = qpolygon;
}

void TestingQObject::setQPolygonF(QPolygonF qpolygonf)
{
    m_qpolygonf = qpolygonf;
}

void TestingQObject::setQKeySequence(QKeySequence qkeysequence)
{
    m_qkeysequence = qkeysequence;
}

void TestingQObject::setTestingObject(TestingQObject *testingObject)
{
    m_testingObject = testingObject;
}

void TestingQObject::setQByteArray(QByteArray qByteArray)
{
    m_qByteArray = qByteArray;
}

void TestingQObject::setQPointF(QPointF qpointf)
{
    m_qpointf = qpointf;
}


QByteArray TestingQObject::qByteArray() const
{
    return m_qByteArray;
}
